<?php

	/**************************************
	*
	*  - Type       : Module
	*  - Document   : Ajax Module
	*  - Developer  : SungMin. Han
	*  - Version    : 2.2
	*
	**************************************/

	class AjaxModule {
		private $microTimer;
		
		public function __construct()
		{
			if($_POST['ajax'] == 't_is_ajax') $this->throwErrorMessage('E001', '정상적인 AJAX 요청이 아닙니다.');
			$this->microTimer = microtime();
		}

		protected function setCheckValue($valueName, $valueData, $errorCode='EC00', $errorMsg=NULL) {
			if(!is_array($valueData)) {
				$valueData = array ($valueData);
				$valueName = array ($valueName);
			}

			if(count($valueData) > 0) {
				foreach($valueData as $key => $val) {
					if(empty($errorMsg)) {
						if(!empty($valueName[$key])) $valueNameItem = $valueName[$key];
						else $valueNameItem = $valueName[0];
						$errorMsg = "$valueNameItem 파라미터가 누락되어 요청하신 서비스가 처리되지 않았습니다.";
					}
					if(!isset($val) || empty($val) || is_null($val)) {
						$this->throwErrorMessage($errorCode, $errorMsg);
					}
				}
			}
			else {
				if(empty($errorMsg)) $errorMsg = "{$valueName[0]} 파라미터가 누락되어 요청하신 서비스가 처리되지 않았습니다.";
				$this->throwErrorMessage($errorCode, $errorMsg);
			}

			if(count($valueData) == 1) {
				$valueData = $valueData[0];
			}
			return $valueData;
		}

		protected function setCheckEmail($email, $txt=null) {
			if(empty($txt)) $txt = '이메일 주소';
			$regex = "/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/"; 
			if(!preg_match($regex, $email)) {
				$this->throwErrorMessage('ET01', $txt . '의 형식이 올바르지 않습니다.');
			}
		}

		protected function setCheckPhone($phone, $txt=null) {
			if(empty($txt)) $txt = '전화번호';
			$regex = "/^[+]?([\d]{0,3})?[\(\.\-\s]?([\d]{3})[\)\.\-\s]*([\d]{3,4})[\.\-\s]?([\d]{4})$/";
			if(!preg_match($regex, $phone)) {
				$this->throwErrorMessage('ET02', $txt . '의 형식이 올바르지 않습니다.');
			}
		}

		protected function setCheckNumber($number, $cast=0, $txt=null) {
			if(empty($txt)) $txt = '숫자 데이터';
			if(!is_numeric($number)) {
				$this->throwErrorMessage('ET03', $txt . '의 형식이 올바르지 않습니다.');
			}

			if(!empty($cast) && $cast > 0) {
				if(strlen(strval($number)) > $cast) {
					$this->throwErrorMessage('ET04', $txt . '는 최대 ' . $cast . '자리까지 입력이 가능합니다.');
				}
			}
		}

		protected function getHostname() {
			$protocol = strtolower(substr($_SERVER["SERVER_PROTOCOL"], 0, strpos( $_SERVER["SERVER_PROTOCOL"], '/'))) . '://';
			$hostname = $protocol . $_SERVER['HTTP_HOST'];
			return $hostname;
		}

		protected function getHostIP() {
			return $_SERVER['REMOTE_ADDR'];
		}

		protected function setJsonData($data, $isError = FALSE, $errorCode = '0000') {
			$rawData  = array();
			$statFlag = ($isError === TRUE)? 'ERROR':'SUCCESS';
			$statCode = ($isError === TRUE)? (($errorCode == '0000')? 'E000':$errorCode):'0000';

			$rawData['response_flag'] = $statFlag;
			$rawData['response_code'] = $statCode;
			$rawData['response_time'] = microtime() - $this->microTimer;
			$rawData['response_date'] = date('Y-m-d H:i:s');

			if(!empty($data)) {
				if(is_array($data) || is_object($data)) {
					$rawData['res'] = $data;
					if(is_array($data)) $rawData['res']['count'] = count((array)$data);
				}
				else {
					$rawData['res']['msg']  = $data;
				}
			}
			else {
				$rawData['res']['msg'] = NULL;
			}

			$this->printJsonData($rawData, $isError);
			$this->microTimer = microtime();
		}

		protected function printJsonData($rawData, $isError = false) {
			if(defined('SET_OUTPUT_HEADER') == FALSE) {
				$this->setJSON();
				define('SET_OUTPUT_HEADER', TRUE);
			}

			if(is_array($rawData)) {
				$encodedData = json_encode($rawData);
			}
			else {
				$this->throwErrorMessage(0, 'API 데이터를 출력하는 중에 예기치 못한 문제가 발생하였습니다.');
			}

			echo $encodedData;
			if($isError === true) exit;
				//@trigger_error($rawData['res']['msg'], E_USER_ERROR); 
		}
		
		protected function throwErrorMessage($errNo = 0, $errMsg, $errConsole=FALSE) {
			if(defined('__AJAX__') && __AJAX__ == TRUE) {
				if(empty($errMsg)) $arrData = '예기치 못한 문제가 발생하였습니다.';
				else $arrData['msg'] = $errMsg;

				if($errNo == 'EL01') $arrData['link'] = '/';
				if(isset($errConsole) && !empty($errConsole)) $arrData['errcon'] = $errConsole;
				$this->setJsonData($arrData, TRUE, $errNo);
			}
			else {
?>
				<script type="text/javascript">
					alert('<?=$errMsg?>');
					location.href = "/";
				</script>
<?php
				die();
			}
		}

		protected function setHTML() {
			header('content-type: text/html; charset=UTF-8');
		}

		protected function setXML() {
			header('content-type: text/xml; charset=UTF-8');
		}

		protected function setJSON() {
			header('content-type: application/json; charset=UTF-8');
		}
	}
?>