<?php

	/**************************************
	*
	*  - Type       : Module
	*  - Document   : Database Module
	*  - Developer  : SungMin. Han
	*  - Version    : 2.6
	*
	**************************************/

	class DatabaseModule
	{
		private $hObjConnection;
		private $oConnectionInfo;
		private $oResultRows;
		private $pathDefaultConfiguration = './conf_database.php';

		public function __construct($mysql_arg=NULL) {
			if(is_object($mysql_arg)) {
				$this->oConnectionInfo->mysql_host     = $mysql_arg->mysql_host;
				$this->oConnectionInfo->mysql_user     = $mysql_arg->mysql_user;
				$this->oConnectionInfo->mysql_password = $mysql_arg->mysql_password;
				$this->oConnectionInfo->mysql_db       = $mysql_arg->mysql_db;
				$this->oConnectionInfo->mysql_charset  = $mysql_arg->mysql_charset;
			}
			else if(!empty($mysql_arg)) {
				$this->setLinkConfigurationDatabase($mysql_arg);
			}
			$this->setConnectionDatabase();
		}

		public function setLinkConfigurationDatabase($mysql_conf) {
			$configurationFilePath = $mysql_conf;
			if(!file_exists($configurationFilePath)) {
				$configurationFilePath = dirname(__FILE__) . '/' . $mysql_conf;
			}

			if(file_exists($configurationFilePath)) {
				@require $configurationFilePath;
				$this->oConnectionInfo->mysql_host     = $mysql_host;
				$this->oConnectionInfo->mysql_user     = $mysql_user;
				$this->oConnectionInfo->mysql_password = $mysql_password;
				$this->oConnectionInfo->mysql_db       = $mysql_db;
				$this->oConnectionInfo->mysql_charset  = $mysql_charset;
			}
		}
		
		public function setConnectionDatabase() {
			if(empty($this->oConnectionInfo->mysql_host) ||
			   empty($this->oConnectionInfo->mysql_user) ||
			   empty($this->oConnectionInfo->mysql_password)) {
				if(file_exists(dirname(__FILE__) . '/' . $this->pathDefaultConfiguration)) {
					$this->setLinkConfigurationDatabase(dirname(__FILE__) . '/' . $this->pathDefaultConfiguration);
				}
				else {
					$this->setConnectionErrorMsg(404, 'Database Configuration not found.');
				}
				return false;
			}
			
			if(!empty($this->oConnectionInfo->mysql_db)) {
				$this->hObjConnection = @new mysqli(	$this->oConnectionInfo->mysql_host,
														$this->oConnectionInfo->mysql_user,
														$this->oConnectionInfo->mysql_password,
														$this->oConnectionInfo->mysql_db);
			}
			else {
				$this->hObjConnection = @new mysqli(	$this->oConnectionInfo->mysql_host,
														$this->oConnectionInfo->mysql_user,
														$this->oConnectionInfo->mysql_password);
			}

			if($this->hObjConnection->errno) {
				$this->setConnectionErrorMsg($this->hObjConnection->errno, $this->hObjConnection->error);
			}

			if(!empty($this->oConnectionInfo->mysql_charset)) {
				$this->setCharset($this->oConnectionInfo->mysql_charset);
			}
			else {
				$this->setCharset('utf8');
			}
		}

		private function getConnection() {
			return @new DatabaseModule;
		}

		private function getConnectionHandle() {
			return $this->hObjConnection;
		}

		private function setConnectionErrorMsg($errno, $error) {
			global $query;
			if('[여기에 어드민 권한 체크 항목을 넣어주세요]' || 1) {
				$message = sprintf("ERROR(%s): %s", $errno, $error).'<br/>'.$query;
			}
			else {
				$message = sprintf("ERROR(%s): Please, Inform to an administrator", $errno);
			}
			die($message);
		}

		public function preventInjection($data) {
			if(!is_array($data)) {
				$data = $this->hObjConnection->real_escape_string($data);
			}
			else {
				if(count($data) > 0) {
					foreach ($data as $key => $val) {
						$data[$key] = $this->preventInjection($val);
					}
				}
			}
			return $data;
		}

		public function setCharset($charset) {
			return $this->query('set names '.$charset);
		}

		public function setDatabase($database) {
			return $this->hObjConnection->select_db($database);
		}

		public function query($query, $bIsCondition=FALSE) {
			if(!isset($query) || empty($query)) {
				$this->setConnectionErrorMsg(0, 'Called an empty query.');
				return false;
			}
			if(is_array($query)) {
				$query = $this->decapsulation($query);
			}
			if(!$result = $this->hObjConnection->query($query)) {
				$this->setConnectionErrorMsg($this->hObjConnection->errno, $this->hObjConnection->error);
			}
			$this->oResultRows->affected_rows = $this->hObjConnection->affected_rows;
			$this->oResultRows->num_rows      = $result->num_rows;
			
			if($bIsCondition === TRUE) {
				if($this->oResultRows->affected_rows > 0 ||
				   $this->oResultRows->num_rows > 0) {
					return true;
				}
				else {
					return false;
				}
			}
			return $result;
		}

		public function aQuery($query, $bIsList=FALSE) {
			if($hQueryResult = $this->query($query)) {
				if($hQueryResult->num_rows) {
					if($bIsList === TRUE) {
						$result = array();
						for($nListIdx=0; $cloneResult=$hQueryResult->fetch_array(); $nListIdx++) {
							$result[$nListIdx] = $cloneResult;
						}
					}
					else {
						$result = $hQueryResult->fetch_array();
						$this->oResultRows->num_rows = $this->oResultRows->affected_rows = 1;
					}
					return $result;
				}
				else {
					return false;
				}
			}
			else {
				$this->setConnectionErrorMsg($this->hObjConnection->errno, $this->hObjConnection->error);
			}
		}

		public function oQuery($query, $bIsList=FALSE) {
			if($hQueryResult = $this->query($query)) {
				if($hQueryResult->num_rows) {
					if($bIsList === TRUE) {
						$result = array();
						for($nListIdx=0; $cloneResult=$hQueryResult->fetch_object(); $nListIdx++) {
							$result[$nListIdx] = $cloneResult;
						}
					}
					else {
						$result = $hQueryResult->fetch_object();
						$this->oResultRows->num_rows = $this->oResultRows->affected_rows = 1;
					}
					return $result;
				}
				else {
					return false;
				}
			}
			else {
				$this->setConnectionErrorMsg($this->hObjConnection->errno, $this->hObjConnection->error);
			}
		}

		public function rQuery($query) {
			$hQueryResult = $this->query($query);
			return $hQueryResult->num_rows;
		}

		public function decapsulation($capsule) {
			$query = '';
			$hash  = '';
			$hash2 = '';
			$loop  = 0;
			$capsule['cmd'] = strtolower($capsule['cmd']);
			if(is_array($capsule['cnm'])) {
				if(!strcmp($capsule['cmd'], 'select')) {
					foreach($capsule['cnm'] as $val) {
						 if($loop != 0) $hash .= ', ';
						 $hash .= $val;
						 $loop++;
					}

					$capsule['cnm'] = $hash;
				}
				else if(!strcmp($capsule['cmd'], 'insert')) {
					foreach($capsule['cnm'] as $key => $val) {
						if($loop != 0){ $hash .= ', '; $hash2 .= ', '; }
						$hash  .= $key;
						if(strpos(strtoupper($val), '@FUNC') !== false) {
							$hash2 .= substr($val, strlen('@FUNC'));
						}
						else {
							$hash2 .= "'" . $val . "'";
						}
						$loop++;
					}

					$capsule['cnm'] = $hash;
					$capsule['val'] = $hash2;
				}
				else if(!strcmp($capsule['cmd'], 'update')) {
					foreach($capsule['cnm'] as $key => $val) {
						if($loop != 0) $hash .= ', ';
						if(strpos($val, '@FUNC') !== false) {
							$hash2 = substr($val, strlen('@FUNC'));
						}
						else {
							$hash2 = "'" . $val . "'";
						}
						$hash .= $key . "=" . $hash2;
						$loop++;
					}

					$capsule['cnm'] = $hash;
				}
			}			

			if(!strcmp($capsule['cmd'], 'select')) {
				$query = $capsule['cmd'] . ' ' . $capsule['cnm'] . ' from ' . $capsule['tbl'] . ' ' . $capsule['whr'];
			}
			else if(!strcmp($capsule['cmd'], 'insert')) {
				$query = $capsule['cmd'] . ' into ' . $capsule['tbl'] . '(' . $capsule['cnm'] . ') values(' . $capsule['val'] . ')';
			}
			else if(!strcmp($capsule['cmd'], 'update')) {
				$query = $capsule['cmd'] . ' ' . $capsule['tbl'] . ' set ' . $capsule['cnm'] . ' ' . $capsule['whr'];
			}
			else if(!strcmp($capsule['cmd'], 'delete')) {
				$query = $capsule['cmd'] . ' from ' . $capsule['tbl'] . ' ' . $capsule['whr'];
			}
			else {
				$query = $capsule['cmd'] . ' ' . $capsule['tbl'];
			}

			return $query;
		}

		public function getAffectedRows() {
			return $this->oResultRows->affected_rows;
		}

		public function getNumRows() {
			return $this->oResultRows->num_rows;
		}

		public function __destruct() {
			return @$this->hObjConnection->close();
		}
	}
?>