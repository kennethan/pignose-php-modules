<?php

	/**************************************
	*
	*  - Type       : Module
	*  - Document   : SMS Module (Gabia Hosted)
	*  - Developer  : SungMin. Han
	*  - Version    : 0.1
	*
	**************************************/

	require_once dirname(__FILE__) . "[가비아 모듈 위치]";
	class ModSMS {
		public function set($return, $send, $msg=null) {
			if(empty($msg)) return 'E001';

			$usrID    = '[가비아 sms 관리자 아이디]';
			$usrKey   = '[가비아 sms 관리자 키]';

			$refKey   = sha1(uniqid(time(), true));

			$api = new gabiaSmsApi($usrID, $usrKey);

			$oBuffer = explode(';', $send);
			$result  = array();

			if(count($oBuffer > 0)) {
				foreach($oBuffer as $p) {
					$r = $api->sms_send($p, $return, $msg, $refKey, null);
					$result[$p] = $r;
				}
			}

			return $result;
		}
	}
?>

